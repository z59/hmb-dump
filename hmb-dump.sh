#!/bin/sh

NVME_DEV=/dev/nvme0
MEM_DEV=/dev/fmem
PAGE_SIZE=4096


set -o errexit    # exit on error
set -o nounset    # fail if var undefined

die() { echo "Error${1:+: }${1-}" >&2; exit 1; }

[ -e "$MEM_DEV" ] || die "$MEM_DEV doesn't exist"

nvme get-feature "$NVME_DEV" -b -f 0x0d | hexdump -e '/4 "%.0s"/8 "%d "/4 "%d\n"' -n 16 | if IFS=' ' read -r HMDLA HMDLEC; then
	echo "HMDLA=$HMDLA HMDLEC=$HMDLEC"
	i=0
	dd "bs=$(( HMDLEC*16 ))" count=1 "if=$MEM_DEV" iflag=skip_bytes "skip=$HMDLA" status=none |
	 hexdump -e '/8 "%d "/4 "%d"/4 "\n"' | while IFS=' ' read -r BADD BSIZE; do
		echo "buffer $i: BADD=$BADD BSIZE=$BSIZE"
		dd "bs=$(( BSIZE*PAGE_SIZE ))" count=1 "if=$MEM_DEV" iflag=skip_bytes "skip=$BADD" status=none | hd >"hmb-${i}.txt"
		i=$(( i+1 ))
	done
fi
